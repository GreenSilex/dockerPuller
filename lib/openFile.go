package lib

import (
	"encoding/json"
	"go.uber.org/zap"
	"io"
	"os"
	"strconv"
)

// setup logging
var logger *zap.SugaredLogger

type fileData struct {
	Ip_str string
	Port   int
}

func OpenFile(jsonFile string, logger *zap.SugaredLogger) [][]string {
	logger.Debug("OpenFile function")

	var address [][]string
	var ipPortSlice []string

	// Open our jsonFile
	fileContent, err := os.Open(jsonFile)

	// if we os.Open returns an error then handle it
	if err != nil {
		logger.Error("error opening: ", jsonFile, ": ", err)
		os.Exit(1)
	}

	logger.Debug("Successfully Opened ", jsonFile)
	// defer the closing of our jsonFile so that we can parse it later on
	defer fileContent.Close()

	// test print file content
	logger.Debug(fileContent)

	// read our opened file as a byte array.
	jsonParser := json.NewDecoder(fileContent)
	for {
		ipPortSlice = nil
		// parse the json looking for the info in the struct
		var m fileData
		if err := jsonParser.Decode(&m); err == io.EOF {
			break
		} else if err != nil {
			logger.Error("error parsing json: ", err)
		}

		// assign ip and port to slice
		ipPortSlice = append(ipPortSlice, m.Ip_str)
		ipPortSlice = append(ipPortSlice, strconv.Itoa(m.Port))

		logger.Debug("Port: ", m.Port)
		logger.Debug("IP address: ", m.Ip_str)

		address = append(address, ipPortSlice)
	}

	logger.Debug("address: ", address)
	return address
}

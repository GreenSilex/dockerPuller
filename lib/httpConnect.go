package lib

import (
	"bytes"
	"crypto/tls"
	//"fmt"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"time"
)

func HttpConnect(ipPort []string, logger *zap.SugaredLogger) {
	var buffer bytes.Buffer

	logger.Info(ipPort)

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	buffer.WriteString("https://")
	buffer.WriteString(ipPort[0])
	buffer.WriteString(":")
	buffer.WriteString(ipPort[1])
	buffer.WriteString("/v2/_catalog")
	logger.Debug("buffer.string: ", buffer.String())

	timeout := time.Duration(3 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	// test https
	resp, err := client.Get(buffer.String())
	if err != nil {
		logger.Debug("err: ", err)
		//return
	} else {
		logger.Debug("Address: ", buffer.String(), "status code: ", resp.StatusCode)
		if resp.StatusCode < 400 || resp.StatusCode > 499 {
			respData, errRead := ioutil.ReadAll(resp.Body)
			if errRead != nil {
				logger.Info("error reading body")
			}
			logger.Info(buffer.String(), ": ", string(respData))
		}

		defer resp.Body.Close()
	}

	// clear buffer
	buffer.Reset()

	// rewrite connection string w/ http
	buffer.WriteString("http://")
	buffer.WriteString(ipPort[0])
	buffer.WriteString(":")
	buffer.WriteString(ipPort[1])
	buffer.WriteString("/v2/_catalog")
	logger.Debug("buffer.string: ", buffer.String())

	// test http
	resp2, err2 := client.Get(buffer.String())
	if err2 != nil {
		logger.Debug("err: ", err2)
		//return
	} else {
		logger.Debug("Address: ", buffer.String(), "status code: ", resp2.StatusCode)
		if resp2.StatusCode < 400 || resp2.StatusCode > 499 {
			resp2Data, errRead2 := ioutil.ReadAll(resp2.Body)
			if errRead2 != nil {
				logger.Info("error reading body")
			}
			logger.Info(buffer.String(), ": ", string(resp2Data))
		}

		defer resp2.Body.Close()
	}
}

package main

import (
	"flag"
	"gitlab.com/greensilex/dockerPuller/lib"
	"gitlab.com/greensilex/zap_log_wrapper"
	"go.uber.org/zap"
	"runtime"
	"sync"
)

var (
	logger *zap.SugaredLogger
	wg     sync.WaitGroup
	level  = "info"
)

func init() {
	flag.StringVar(&level, "log-level", level, "potential values: debug, info, warn, error")
}

func main() {
	runtime.GOMAXPROCS(2) // set the number of CPUs to use

	var jsonFile string

	flag.StringVar(&jsonFile, "file", "test.json", "json file to parse")

	// initialize the command line variables
	flag.Parse()

	// setup logging
	logger := greensilexZapWrapper.LogSetup(level)

	logger.Info("Starting dockerPuller")
	logger.Debug("file name: ", jsonFile)

	ipPortSlice := lib.OpenFile(jsonFile, logger)
	logger.Debug("main: ", ipPortSlice)

	// loop through slice
	for i := 0; i < len(ipPortSlice); i++ {
		lib.HttpConnect(ipPortSlice[i], logger)
	}
}
